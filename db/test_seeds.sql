--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO abdennour;

--
-- Name: characters; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.characters (
    id bigint NOT NULL,
    name character varying,
    birth_year character varying,
    eye_color character varying,
    gender character varying,
    hair_color character varying,
    height character varying,
    skin_color character varying,
    mass character varying,
    planet_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.characters OWNER TO abdennour;

--
-- Name: characters_films; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.characters_films (
    character_id bigint NOT NULL,
    film_id bigint NOT NULL
);


ALTER TABLE public.characters_films OWNER TO abdennour;

--
-- Name: characters_id_seq; Type: SEQUENCE; Schema: public; Owner: abdennour
--

CREATE SEQUENCE public.characters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.characters_id_seq OWNER TO abdennour;

--
-- Name: characters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abdennour
--

ALTER SEQUENCE public.characters_id_seq OWNED BY public.characters.id;


--
-- Name: characters_species; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.characters_species (
    character_id bigint NOT NULL,
    specie_id bigint NOT NULL
);


ALTER TABLE public.characters_species OWNER TO abdennour;

--
-- Name: films; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.films (
    id bigint NOT NULL,
    title character varying,
    episode_id integer,
    opening_crawl character varying,
    director character varying,
    producer character varying,
    release_date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.films OWNER TO abdennour;

--
-- Name: films_id_seq; Type: SEQUENCE; Schema: public; Owner: abdennour
--

CREATE SEQUENCE public.films_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.films_id_seq OWNER TO abdennour;

--
-- Name: films_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abdennour
--

ALTER SEQUENCE public.films_id_seq OWNED BY public.films.id;


--
-- Name: films_planets; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.films_planets (
    planet_id bigint NOT NULL,
    film_id bigint NOT NULL
);


ALTER TABLE public.films_planets OWNER TO abdennour;

--
-- Name: planets; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.planets (
    id bigint NOT NULL,
    name character varying,
    diameter character varying,
    rotation_period character varying,
    orbital_period character varying,
    gravity character varying,
    population character varying,
    climate character varying,
    terrain character varying,
    surface_water character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.planets OWNER TO abdennour;

--
-- Name: planets_id_seq; Type: SEQUENCE; Schema: public; Owner: abdennour
--

CREATE SEQUENCE public.planets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planets_id_seq OWNER TO abdennour;

--
-- Name: planets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abdennour
--

ALTER SEQUENCE public.planets_id_seq OWNED BY public.planets.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO abdennour;

--
-- Name: species; Type: TABLE; Schema: public; Owner: abdennour
--

CREATE TABLE public.species (
    id bigint NOT NULL,
    name character varying,
    classification character varying,
    designation character varying,
    average_height character varying,
    average_lifespan character varying,
    eye_colors character varying,
    hair_colors character varying,
    skin_colors character varying,
    language character varying,
    planet_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.species OWNER TO abdennour;

--
-- Name: species_id_seq; Type: SEQUENCE; Schema: public; Owner: abdennour
--

CREATE SEQUENCE public.species_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.species_id_seq OWNER TO abdennour;

--
-- Name: species_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abdennour
--

ALTER SEQUENCE public.species_id_seq OWNED BY public.species.id;


--
-- Name: characters id; Type: DEFAULT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.characters ALTER COLUMN id SET DEFAULT nextval('public.characters_id_seq'::regclass);


--
-- Name: films id; Type: DEFAULT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.films ALTER COLUMN id SET DEFAULT nextval('public.films_id_seq'::regclass);


--
-- Name: planets id; Type: DEFAULT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.planets ALTER COLUMN id SET DEFAULT nextval('public.planets_id_seq'::regclass);


--
-- Name: species id; Type: DEFAULT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.species ALTER COLUMN id SET DEFAULT nextval('public.species_id_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-07-04 14:32:31.240092	2018-07-04 14:32:31.240092
\.


--
-- Data for Name: characters; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.characters (id, name, birth_year, eye_color, gender, hair_color, height, skin_color, mass, planet_id, created_at, updated_at) FROM stdin;
1	Luke Skywalker	19BBY	blue	male	blond	172	fair	77	60	2018-07-04 14:33:07.05475	2018-07-04 14:33:07.05475
2	C-3PO	112BBY	yellow	n/a	n/a	167	gold	75	60	2018-07-04 14:33:08.963421	2018-07-04 14:33:08.963421
3	R2-D2	33BBY	red	n/a	n/a	96	white, blue	32	7	2018-07-04 14:33:11.567121	2018-07-04 14:33:11.567121
4	Darth Vader	41.9BBY	yellow	male	none	202	white	136	60	2018-07-04 14:33:13.412292	2018-07-04 14:33:13.412292
5	Leia Organa	19BBY	brown	female	brown	150	light	49	1	2018-07-04 14:33:15.562569	2018-07-04 14:33:15.562569
6	Owen Lars	52BBY	blue	male	brown, grey	178	light	120	60	2018-07-04 14:33:17.09732	2018-07-04 14:33:17.09732
7	Beru Whitesun lars	47BBY	blue	female	brown	165	light	75	60	2018-07-04 14:33:19.489079	2018-07-04 14:33:19.489079
8	R5-D4	unknown	red	n/a	n/a	97	white, red	32	60	2018-07-04 14:33:21.711044	2018-07-04 14:33:21.711044
9	Biggs Darklighter	24BBY	brown	male	black	183	light	84	60	2018-07-04 14:33:24.470724	2018-07-04 14:33:24.470724
10	Obi-Wan Kenobi	57BBY	blue-gray	male	auburn, white	182	fair	77	19	2018-07-04 14:33:26.315708	2018-07-04 14:33:26.315708
11	Anakin Skywalker	41.9BBY	blue	male	blond	188	fair	84	60	2018-07-04 14:33:28.771722	2018-07-04 14:33:28.771722
12	Wilhuff Tarkin	64BBY	blue	male	auburn, grey	180	fair	unknown	20	2018-07-04 14:33:30.922196	2018-07-04 14:33:30.922196
13	Chewbacca	200BBY	blue	male	brown	228	unknown	112	13	2018-07-04 14:33:32.765188	2018-07-04 14:33:32.765188
14	Han Solo	29BBY	brown	male	brown	180	fair	80	21	2018-07-04 14:33:34.60852	2018-07-04 14:33:34.60852
15	Greedo	44BBY	black	male	n/a	173	green	74	22	2018-07-04 14:33:37.06629	2018-07-04 14:33:37.06629
16	Jabba Desilijic Tiure	600BBY	orange	hermaphrodite	n/a	175	green-tan, brown	1,358	23	2018-07-04 14:33:39.831563	2018-07-04 14:33:39.831563
17	Wedge Antilles	21BBY	hazel	male	brown	170	fair	77	21	2018-07-04 14:33:41.673044	2018-07-04 14:33:41.673044
18	Jek Tono Porkins	unknown	blue	male	brown	180	fair	110	25	2018-07-04 14:33:43.517507	2018-07-04 14:33:43.517507
19	Yoda	896BBY	brown	male	white	66	green	17	27	2018-07-04 14:33:45.05679	2018-07-04 14:33:45.05679
20	Palpatine	82BBY	yellow	male	grey	170	pale	75	7	2018-07-04 14:33:46.897224	2018-07-04 14:33:46.897224
21	Boba Fett	31.5BBY	brown	male	black	183	fair	78.2	9	2018-07-04 14:33:49.355396	2018-07-04 14:33:49.355396
22	IG-88	15BBY	red	none	none	200	metal	140	27	2018-07-04 14:33:51.197222	2018-07-04 14:33:51.197222
23	Bossk	53BBY	red	male	none	190	green	113	28	2018-07-04 14:33:53.040779	2018-07-04 14:33:53.040779
24	Lando Calrissian	31BBY	brown	male	black	177	dark	79	29	2018-07-04 14:33:54.880262	2018-07-04 14:33:54.880262
25	Lobot	37BBY	blue	male	none	175	light	79	5	2018-07-04 14:33:56.707843	2018-07-04 14:33:56.707843
26	Ackbar	41BBY	orange	male	none	180	brown mottle	83	30	2018-07-04 14:33:58.336147	2018-07-04 14:33:58.336147
27	Mon Mothma	48BBY	blue	female	auburn	150	fair	unknown	31	2018-07-04 14:34:00.414249	2018-07-04 14:34:00.414249
28	Arvel Crynyd	unknown	brown	male	brown	unknown	fair	unknown	27	2018-07-04 14:34:02.255444	2018-07-04 14:34:02.255444
29	Wicket Systri Warrick	8BBY	brown	male	brown	88	brown	20	6	2018-07-04 14:34:04.102293	2018-07-04 14:34:04.102293
30	Nien Nunb	unknown	black	male	none	160	grey	68	32	2018-07-04 14:34:05.636745	2018-07-04 14:34:05.636745
31	Qui-Gon Jinn	92BBY	blue	male	brown	193	fair	89	27	2018-07-04 14:34:08.52836	2018-07-04 14:34:08.52836
32	Nute Gunray	unknown	red	male	none	191	mottled green	90	17	2018-07-04 14:34:10.552355	2018-07-04 14:34:10.552355
33	Finis Valorum	91BBY	blue	male	blond	170	fair	unknown	8	2018-07-04 14:34:12.08902	2018-07-04 14:34:12.08902
34	Jar Jar Binks	52BBY	orange	male	none	196	orange	66	7	2018-07-04 14:34:13.930647	2018-07-04 14:34:13.930647
35	Roos Tarpals	unknown	orange	male	none	224	grey	82	7	2018-07-04 14:34:15.769359	2018-07-04 14:34:15.769359
36	Rugor Nass	unknown	orange	male	none	206	green	unknown	7	2018-07-04 14:34:18.840646	2018-07-04 14:34:18.840646
37	Ric Olié	unknown	blue	male	brown	183	fair	unknown	7	2018-07-04 14:34:20.380709	2018-07-04 14:34:20.380709
38	Watto	unknown	yellow	male	black	137	blue, grey	unknown	33	2018-07-04 14:34:21.91965	2018-07-04 14:34:21.91965
39	Sebulba	unknown	orange	male	none	112	grey, red	40	34	2018-07-04 14:34:23.768611	2018-07-04 14:34:23.768611
40	Quarsh Panaka	62BBY	brown	male	black	183	dark	unknown	7	2018-07-04 14:34:24.904019	2018-07-04 14:34:24.904019
41	Shmi Skywalker	72BBY	brown	female	black	163	fair	unknown	60	2018-07-04 14:34:27.451017	2018-07-04 14:34:27.451017
42	Darth Maul	54BBY	yellow	male	none	175	red	80	35	2018-07-04 14:34:29.290654	2018-07-04 14:34:29.290654
43	Bib Fortuna	unknown	pink	male	none	180	pale	unknown	36	2018-07-04 14:34:31.139665	2018-07-04 14:34:31.139665
44	Ayla Secura	48BBY	hazel	female	none	178	blue	55	36	2018-07-04 14:34:32.572361	2018-07-04 14:34:32.572361
45	Dud Bolt	unknown	yellow	male	none	94	blue, grey	45	38	2018-07-04 14:34:34.510349	2018-07-04 14:34:34.510349
46	Gasgano	unknown	black	male	none	122	white, blue	unknown	39	2018-07-04 14:34:36.356718	2018-07-04 14:34:36.356718
47	Ben Quadinaros	unknown	orange	male	none	163	grey, green, yellow	65	40	2018-07-04 14:34:37.893065	2018-07-04 14:34:37.893065
48	Mace Windu	72BBY	brown	male	none	188	dark	84	41	2018-07-04 14:34:39.737585	2018-07-04 14:34:39.737585
49	Ki-Adi-Mundi	92BBY	yellow	male	white	198	pale	82	42	2018-07-04 14:34:41.556716	2018-07-04 14:34:41.556716
50	Kit Fisto	unknown	black	male	none	196	green	87	43	2018-07-04 14:34:43.416963	2018-07-04 14:34:43.416963
51	Eeth Koth	unknown	brown	male	black	171	brown	unknown	44	2018-07-04 14:34:45.879995	2018-07-04 14:34:45.879995
52	Adi Gallia	unknown	blue	female	none	184	dark	50	8	2018-07-04 14:34:47.724278	2018-07-04 14:34:47.724278
53	Saesee Tiin	unknown	orange	male	none	188	pale	unknown	46	2018-07-04 14:34:49.566883	2018-07-04 14:34:49.566883
54	Yarael Poof	unknown	yellow	male	none	264	white	unknown	47	2018-07-04 14:34:51.408557	2018-07-04 14:34:51.408557
55	Plo Koon	22BBY	black	male	none	188	orange	80	48	2018-07-04 14:34:52.8466	2018-07-04 14:34:52.8466
56	Mas Amedda	unknown	blue	male	none	196	blue	unknown	49	2018-07-04 14:34:54.206979	2018-07-04 14:34:54.206979
57	Gregar Typho	unknown	brown	male	black	185	dark	85	7	2018-07-04 14:34:55.710099	2018-07-04 14:34:55.710099
58	Cordé	unknown	brown	female	brown	157	light	unknown	7	2018-07-04 14:34:57.240942	2018-07-04 14:34:57.240942
59	Cliegg Lars	82BBY	blue	male	brown	183	fair	unknown	60	2018-07-04 14:34:58.781748	2018-07-04 14:34:58.781748
60	Poggle the Lesser	unknown	yellow	male	none	183	green	80	10	2018-07-04 14:34:59.930322	2018-07-04 14:34:59.930322
61	Luminara Unduli	58BBY	blue	female	black	170	yellow	56.2	50	2018-07-04 14:35:02.162216	2018-07-04 14:35:02.162216
62	Barriss Offee	40BBY	blue	female	black	166	yellow	50	50	2018-07-04 14:35:04.006524	2018-07-04 14:35:04.006524
63	Dormé	unknown	brown	female	brown	165	light	unknown	7	2018-07-04 14:35:05.437633	2018-07-04 14:35:05.437633
64	Dooku	102BBY	brown	male	white	193	fair	80	51	2018-07-04 14:35:07.691175	2018-07-04 14:35:07.691175
65	Bail Prestor Organa	67BBY	brown	male	black	191	tan	unknown	1	2018-07-04 14:35:09.125314	2018-07-04 14:35:09.125314
66	Jango Fett	66BBY	brown	male	black	183	tan	79	52	2018-07-04 14:35:10.764177	2018-07-04 14:35:10.764177
67	Zam Wesell	unknown	yellow	female	blonde	168	fair, green, yellow	55	53	2018-07-04 14:35:12.249456	2018-07-04 14:35:12.249456
68	Dexter Jettster	unknown	yellow	male	none	198	brown	102	54	2018-07-04 14:35:13.528174	2018-07-04 14:35:13.528174
69	Lama Su	unknown	black	male	none	229	grey	88	9	2018-07-04 14:35:15.98593	2018-07-04 14:35:15.98593
70	Taun We	unknown	black	female	none	213	grey	unknown	9	2018-07-04 14:35:17.516602	2018-07-04 14:35:17.516602
71	Jocasta Nu	unknown	blue	female	white	167	fair	unknown	8	2018-07-04 14:35:20.252323	2018-07-04 14:35:20.252323
72	Ratts Tyerell	unknown	unknown	male	none	79	grey, blue	15	37	2018-07-04 14:35:22.060042	2018-07-04 14:35:22.060042
73	R4-P17	unknown	red, blue	female	none	96	silver, red	unknown	27	2018-07-04 14:35:23.35832	2018-07-04 14:35:23.35832
74	Wat Tambor	unknown	unknown	male	none	193	green, grey	48	55	2018-07-04 14:35:25.20113	2018-07-04 14:35:25.20113
75	San Hill	unknown	gold	male	none	191	grey	unknown	56	2018-07-04 14:35:27.045508	2018-07-04 14:35:27.045508
76	Shaak Ti	unknown	black	female	none	178	red, blue, white	57	57	2018-07-04 14:35:28.888671	2018-07-04 14:35:28.888671
77	Grievous	unknown	green, yellow	male	none	216	brown, white	159	58	2018-07-04 14:35:30.378396	2018-07-04 14:35:30.378396
78	Tarfful	unknown	blue	male	brown	234	brown	136	13	2018-07-04 14:35:31.862443	2018-07-04 14:35:31.862443
79	Raymus Antilles	unknown	brown	male	brown	188	light	79	1	2018-07-04 14:35:33.495956	2018-07-04 14:35:33.495956
80	Sly Moore	unknown	white	female	none	178	pale	48	59	2018-07-04 14:35:34.722068	2018-07-04 14:35:34.722068
81	Tion Medon	unknown	black	male	none	206	grey	80	11	2018-07-04 14:35:37.183014	2018-07-04 14:35:37.183014
82	Finn	unknown	dark	male	black	unknown	dark	unknown	27	2018-07-04 14:35:38.719537	2018-07-04 14:35:38.719537
83	Rey	unknown	hazel	female	brown	unknown	light	unknown	27	2018-07-04 14:35:40.256333	2018-07-04 14:35:40.256333
84	Poe Dameron	unknown	brown	male	brown	unknown	light	unknown	27	2018-07-04 14:35:42.069005	2018-07-04 14:35:42.069005
85	BB8	unknown	black	none	none	unknown	none	unknown	27	2018-07-04 14:35:43.638045	2018-07-04 14:35:43.638045
86	Captain Phasma	unknown	unknown	female	unknown	unknown	unknown	unknown	27	2018-07-04 14:35:44.859848	2018-07-04 14:35:44.859848
87	Padmé Amidala	46BBY	brown	female	brown	165	light	45	7	2018-07-04 14:35:46.70616	2018-07-04 14:35:46.70616
\.


--
-- Data for Name: characters_films; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.characters_films (character_id, film_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
79	1
2	2
3	2
6	2
7	2
10	2
11	2
19	2
20	2
21	2
32	2
34	2
38	2
41	2
44	2
48	2
49	2
50	2
55	2
56	2
57	2
58	2
59	2
60	2
61	2
62	2
63	2
64	2
65	2
66	2
67	2
68	2
69	2
70	2
71	2
73	2
74	2
75	2
76	2
80	2
87	2
2	3
3	3
10	3
11	3
16	3
19	3
20	3
31	3
32	3
33	3
34	3
35	3
36	3
37	3
38	3
39	3
40	3
41	3
42	3
44	3
45	3
46	3
47	3
48	3
49	3
50	3
51	3
52	3
53	3
54	3
55	3
56	3
72	3
87	3
1	4
2	4
3	4
4	4
5	4
6	4
7	4
10	4
11	4
12	4
13	4
19	4
20	4
32	4
44	4
48	4
49	4
50	4
51	4
52	4
53	4
55	4
60	4
61	4
64	4
65	4
73	4
76	4
77	4
78	4
79	4
80	4
81	4
87	4
1	5
2	5
3	5
4	5
5	5
10	5
13	5
14	5
16	5
17	5
19	5
20	5
21	5
24	5
26	5
27	5
28	5
29	5
30	5
43	5
1	6
2	6
3	6
4	6
5	6
10	6
13	6
14	6
17	6
19	6
20	6
21	6
22	6
23	6
24	6
25	6
1	7
3	7
5	7
13	7
14	7
26	7
82	7
83	7
84	7
85	7
86	7
\.


--
-- Name: characters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abdennour
--

SELECT pg_catalog.setval('public.characters_id_seq', 87, true);


--
-- Data for Name: characters_species; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.characters_species (character_id, specie_id) FROM stdin;
1	36
2	35
3	35
4	36
5	36
6	36
7	36
8	35
9	36
10	36
11	36
12	36
13	34
14	36
15	37
16	1
17	36
18	36
19	2
20	36
21	36
22	35
23	3
24	36
25	36
26	4
27	36
28	36
29	5
30	6
31	36
32	7
33	36
34	8
35	8
36	8
38	9
39	10
41	36
42	18
43	11
44	11
45	13
46	14
47	15
48	36
49	16
50	17
51	18
52	19
53	20
54	21
55	22
56	23
57	36
58	36
59	36
60	24
61	25
62	25
63	36
64	36
65	36
66	36
67	26
68	27
69	28
70	28
71	36
72	12
74	29
75	30
76	31
77	32
78	34
79	36
81	33
82	36
83	36
84	36
85	35
87	36
\.


--
-- Data for Name: films; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.films (id, title, episode_id, opening_crawl, director, producer, release_date, created_at, updated_at) FROM stdin;
1	A New Hope	4	It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....	George Lucas	Gary Kurtz, Rick McCallum	1977-05-25	2018-07-04 14:36:00.867471	2018-07-04 14:36:00.867471
2	Attack of the Clones	2	There is unrest in the Galactic\r\nSenate. Several thousand solar\r\nsystems have declared their\r\nintentions to leave the Republic.\r\n\r\nThis separatist movement,\r\nunder the leadership of the\r\nmysterious Count Dooku, has\r\nmade it difficult for the limited\r\nnumber of Jedi Knights to maintain \r\npeace and order in the galaxy.\r\n\r\nSenator Amidala, the former\r\nQueen of Naboo, is returning\r\nto the Galactic Senate to vote\r\non the critical issue of creating\r\nan ARMY OF THE REPUBLIC\r\nto assist the overwhelmed\r\nJedi....	George Lucas	Rick McCallum	2002-05-16	2018-07-04 14:36:26.655438	2018-07-04 14:36:26.655438
3	The Phantom Menace	1	Turmoil has engulfed the\r\nGalactic Republic. The taxation\r\nof trade routes to outlying star\r\nsystems is in dispute.\r\n\r\nHoping to resolve the matter\r\nwith a blockade of deadly\r\nbattleships, the greedy Trade\r\nFederation has stopped all\r\nshipping to the small planet\r\nof Naboo.\r\n\r\nWhile the Congress of the\r\nRepublic endlessly debates\r\nthis alarming chain of events,\r\nthe Supreme Chancellor has\r\nsecretly dispatched two Jedi\r\nKnights, the guardians of\r\npeace and justice in the\r\ngalaxy, to settle the conflict....	George Lucas	Rick McCallum	1999-05-19	2018-07-04 14:36:46.029765	2018-07-04 14:36:46.029765
4	Revenge of the Sith	3	War! The Republic is crumbling\r\nunder attacks by the ruthless\r\nSith Lord, Count Dooku.\r\nThere are heroes on both sides.\r\nEvil is everywhere.\r\n\r\nIn a stunning move, the\r\nfiendish droid leader, General\r\nGrievous, has swept into the\r\nRepublic capital and kidnapped\r\nChancellor Palpatine, leader of\r\nthe Galactic Senate.\r\n\r\nAs the Separatist Droid Army\r\nattempts to flee the besieged\r\ncapital with their valuable\r\nhostage, two Jedi Knights lead a\r\ndesperate mission to rescue the\r\ncaptive Chancellor....	George Lucas	Rick McCallum	2005-05-19	2018-07-04 14:37:12.555084	2018-07-04 14:37:12.555084
5	Return of the Jedi	6	Luke Skywalker has returned to\r\nhis home planet of Tatooine in\r\nan attempt to rescue his\r\nfriend Han Solo from the\r\nclutches of the vile gangster\r\nJabba the Hutt.\r\n\r\nLittle does Luke know that the\r\nGALACTIC EMPIRE has secretly\r\nbegun construction on a new\r\narmored space station even\r\nmore powerful than the first\r\ndreaded Death Star.\r\n\r\nWhen completed, this ultimate\r\nweapon will spell certain doom\r\nfor the small band of rebels\r\nstruggling to restore freedom\r\nto the galaxy...	Richard Marquand	Howard G. Kazanjian, George Lucas, Rick McCallum	1983-05-25	2018-07-04 14:37:27.794417	2018-07-04 14:37:27.794417
6	The Empire Strikes Back	5	It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed,\r\nImperial troops have driven the\r\nRebel forces from their hidden\r\nbase and pursued them across\r\nthe galaxy.\r\n\r\nEvading the dreaded Imperial\r\nStarfleet, a group of freedom\r\nfighters led by Luke Skywalker\r\nhas established a new secret\r\nbase on the remote ice world\r\nof Hoth.\r\n\r\nThe evil lord Darth Vader,\r\nobsessed with finding young\r\nSkywalker, has dispatched\r\nthousands of remote probes into\r\nthe far reaches of space....	Irvin Kershner	Gary Kurtz, Rick McCallum	1980-05-17	2018-07-04 14:37:42.520285	2018-07-04 14:37:42.520285
7	The Force Awakens	7	Luke Skywalker has vanished.\r\nIn his absence, the sinister\r\nFIRST ORDER has risen from\r\nthe ashes of the Empire\r\nand will not rest until\r\nSkywalker, the last Jedi,\r\nhas been destroyed.\r\n \r\nWith the support of the\r\nREPUBLIC, General Leia Organa\r\nleads a brave RESISTANCE.\r\nShe is desperate to find her\r\nbrother Luke and gain his\r\nhelp in restoring peace and\r\njustice to the galaxy.\r\n \r\nLeia has sent her most daring\r\npilot on a secret mission\r\nto Jakku, where an old ally\r\nhas discovered a clue to\r\nLuke's whereabouts....	J. J. Abrams	Kathleen Kennedy, J. J. Abrams, Bryan Burk	2015-12-11	2018-07-04 14:37:49.589833	2018-07-04 14:37:49.589833
\.


--
-- Name: films_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abdennour
--

SELECT pg_catalog.setval('public.films_id_seq', 7, true);


--
-- Data for Name: films_planets; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.films_planets (planet_id, film_id) FROM stdin;
1	1
2	1
60	1
7	2
8	2
9	2
10	2
60	2
7	3
8	3
60	3
1	4
4	4
7	4
8	4
11	4
12	4
13	4
14	4
15	4
16	4
17	4
18	4
60	4
4	5
6	5
7	5
8	5
60	5
3	6
4	6
5	6
26	6
61	7
\.


--
-- Data for Name: planets; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.planets (id, name, diameter, rotation_period, orbital_period, gravity, population, climate, terrain, surface_water, created_at, updated_at) FROM stdin;
1	Alderaan	12500	24	364	1 standard	2000000000	temperate	grasslands, mountains	40	2018-07-04 14:32:33.215438	2018-07-04 14:32:33.215438
2	Yavin IV	10200	24	4818	1 standard	1000	temperate, tropical	jungle, rainforests	8	2018-07-04 14:32:33.219565	2018-07-04 14:32:33.219565
3	Hoth	7200	23	549	1.1 standard	unknown	frozen	tundra, ice caves, mountain ranges	100	2018-07-04 14:32:33.222541	2018-07-04 14:32:33.222541
4	Dagobah	8900	23	341	N/A	unknown	murky	swamp, jungles	8	2018-07-04 14:32:33.225197	2018-07-04 14:32:33.225197
5	Bespin	118000	12	5110	1.5 (surface), 1 standard (Cloud City)	6000000	temperate	gas giant	0	2018-07-04 14:32:33.228189	2018-07-04 14:32:33.228189
6	Endor	4900	18	402	0.85 standard	30000000	temperate	forests, mountains, lakes	8	2018-07-04 14:32:33.231096	2018-07-04 14:32:33.231096
7	Naboo	12120	26	312	1 standard	4500000000	temperate	grassy hills, swamps, forests, mountains	12	2018-07-04 14:32:33.233816	2018-07-04 14:32:33.233816
8	Coruscant	12240	24	368	1 standard	1000000000000	temperate	cityscape, mountains	unknown	2018-07-04 14:32:33.236167	2018-07-04 14:32:33.236167
9	Kamino	19720	27	463	1 standard	1000000000	temperate	ocean	100	2018-07-04 14:32:33.238684	2018-07-04 14:32:33.238684
10	Geonosis	11370	30	256	0.9 standard	100000000000	temperate, arid	rock, desert, mountain, barren	5	2018-07-04 14:32:33.241195	2018-07-04 14:32:33.241195
11	Utapau	12900	27	351	1 standard	95000000	temperate, arid, windy	scrublands, savanna, canyons, sinkholes	0.9	2018-07-04 14:32:33.983017	2018-07-04 14:32:33.983017
12	Mustafar	4200	36	412	1 standard	20000	hot	volcanoes, lava rivers, mountains, caves	0	2018-07-04 14:32:33.991224	2018-07-04 14:32:33.991224
13	Kashyyyk	12765	26	381	1 standard	45000000	tropical	jungle, forests, lakes, rivers	60	2018-07-04 14:32:33.997532	2018-07-04 14:32:33.997532
14	Polis Massa	0	24	590	0.56 standard	1000000	artificial temperate 	airless asteroid	0	2018-07-04 14:32:34.004355	2018-07-04 14:32:34.004355
15	Mygeeto	10088	12	167	1 standard	19000000	frigid	glaciers, mountains, ice canyons	unknown	2018-07-04 14:32:34.010476	2018-07-04 14:32:34.010476
16	Felucia	9100	34	231	0.75 standard	8500000	hot, humid	fungus forests	unknown	2018-07-04 14:32:34.017264	2018-07-04 14:32:34.017264
17	Cato Neimoidia	0	25	278	1 standard	10000000	temperate, moist	mountains, fields, forests, rock arches	unknown	2018-07-04 14:32:34.023454	2018-07-04 14:32:34.023454
18	Saleucami	14920	26	392	unknown	1400000000	hot	caves, desert, mountains, volcanoes	unknown	2018-07-04 14:32:34.028237	2018-07-04 14:32:34.028237
19	Stewjon	0	unknown	unknown	1 standard	unknown	temperate	grass	unknown	2018-07-04 14:32:34.032389	2018-07-04 14:32:34.032389
20	Eriadu	13490	24	360	1 standard	22000000000	polluted	cityscape	unknown	2018-07-04 14:32:34.035922	2018-07-04 14:32:34.035922
21	Corellia	11000	25	329	1 standard	3000000000	temperate	plains, urban, hills, forests	70	2018-07-04 14:32:34.699491	2018-07-04 14:32:34.699491
22	Rodia	7549	29	305	1 standard	1300000000	hot	jungles, oceans, urban, swamps	60	2018-07-04 14:32:34.704572	2018-07-04 14:32:34.704572
23	Nal Hutta	12150	87	413	1 standard	7000000000	temperate	urban, oceans, swamps, bogs	unknown	2018-07-04 14:32:34.706944	2018-07-04 14:32:34.706944
24	Dantooine	9830	25	378	1 standard	1000	temperate	oceans, savannas, mountains, grasslands	unknown	2018-07-04 14:32:34.70927	2018-07-04 14:32:34.70927
25	Bestine IV	6400	26	680	unknown	62000000	temperate	rocky islands, oceans	98	2018-07-04 14:32:34.712086	2018-07-04 14:32:34.712086
26	Ord Mantell	14050	26	334	1 standard	4000000000	temperate	plains, seas, mesas	10	2018-07-04 14:32:34.714671	2018-07-04 14:32:34.714671
27	unknown	0	0	0	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:34.717624	2018-07-04 14:32:34.717624
28	Trandosha	0	25	371	0.62 standard	42000000	arid	mountains, seas, grasslands, deserts	unknown	2018-07-04 14:32:34.720509	2018-07-04 14:32:34.720509
29	Socorro	0	20	326	1 standard	300000000	arid	deserts, mountains	unknown	2018-07-04 14:32:34.72328	2018-07-04 14:32:34.72328
30	Mon Cala	11030	21	398	1	27000000000	temperate	oceans, reefs, islands	100	2018-07-04 14:32:34.72609	2018-07-04 14:32:34.72609
31	Chandrila	13500	20	368	1	1200000000	temperate	plains, forests	40	2018-07-04 14:32:35.313905	2018-07-04 14:32:35.313905
32	Sullust	12780	20	263	1	18500000000	superheated	mountains, volcanoes, rocky deserts	5	2018-07-04 14:32:35.318604	2018-07-04 14:32:35.318604
33	Toydaria	7900	21	184	1	11000000	temperate	swamps, lakes	unknown	2018-07-04 14:32:35.322288	2018-07-04 14:32:35.322288
34	Malastare	18880	26	201	1.56	2000000000	arid, temperate, tropical	swamps, deserts, jungles, mountains	unknown	2018-07-04 14:32:35.325892	2018-07-04 14:32:35.325892
35	Dathomir	10480	24	491	0.9	5200	temperate	forests, deserts, savannas	unknown	2018-07-04 14:32:35.330329	2018-07-04 14:32:35.330329
36	Ryloth	10600	30	305	1	1500000000	temperate, arid, subartic	mountains, valleys, deserts, tundra	5	2018-07-04 14:32:35.334342	2018-07-04 14:32:35.334342
37	Aleen Minor	unknown	unknown	unknown	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:35.339495	2018-07-04 14:32:35.339495
38	Vulpter	14900	22	391	1	421000000	temperate, artic	urban, barren	unknown	2018-07-04 14:32:35.343788	2018-07-04 14:32:35.343788
39	Troiken	unknown	unknown	unknown	unknown	unknown	unknown	desert, tundra, rainforests, mountains	unknown	2018-07-04 14:32:35.348445	2018-07-04 14:32:35.348445
40	Tund	12190	48	1770	unknown	0	unknown	barren, ash	unknown	2018-07-04 14:32:35.352193	2018-07-04 14:32:35.352193
41	Haruun Kal	10120	25	383	0.98	705300	temperate	toxic cloudsea, plateaus, volcanoes	unknown	2018-07-04 14:32:35.911706	2018-07-04 14:32:35.911706
42	Cerea	unknown	27	386	1	450000000	temperate	verdant	20	2018-07-04 14:32:35.94103	2018-07-04 14:32:35.94103
43	Glee Anselm	15600	33	206	1	500000000	tropical, temperate	lakes, islands, swamps, seas	80	2018-07-04 14:32:35.948323	2018-07-04 14:32:35.948323
44	Iridonia	unknown	29	413	unknown	unknown	unknown	rocky canyons, acid pools	unknown	2018-07-04 14:32:35.953739	2018-07-04 14:32:35.953739
45	Tholoth	unknown	unknown	unknown	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:35.957837	2018-07-04 14:32:35.957837
46	Iktotch	unknown	22	481	1	unknown	arid, rocky, windy	rocky	unknown	2018-07-04 14:32:35.961373	2018-07-04 14:32:35.961373
47	Quermia	unknown	unknown	unknown	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:35.964384	2018-07-04 14:32:35.964384
48	Dorin	13400	22	409	1	unknown	temperate	unknown	unknown	2018-07-04 14:32:35.968598	2018-07-04 14:32:35.968598
49	Champala	unknown	27	318	1	3500000000	temperate	oceans, rainforests, plateaus	unknown	2018-07-04 14:32:35.974103	2018-07-04 14:32:35.974103
50	Mirial	unknown	unknown	unknown	unknown	unknown	unknown	deserts	unknown	2018-07-04 14:32:35.978058	2018-07-04 14:32:35.978058
51	Serenno	unknown	unknown	unknown	unknown	unknown	unknown	rainforests, rivers, mountains	unknown	2018-07-04 14:32:36.543237	2018-07-04 14:32:36.543237
52	Concord Dawn	unknown	unknown	unknown	unknown	unknown	unknown	jungles, forests, deserts	unknown	2018-07-04 14:32:36.551412	2018-07-04 14:32:36.551412
53	Zolan	unknown	unknown	unknown	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:36.558156	2018-07-04 14:32:36.558156
54	Ojom	unknown	unknown	unknown	unknown	500000000	frigid	oceans, glaciers	100	2018-07-04 14:32:36.564959	2018-07-04 14:32:36.564959
55	Skako	unknown	27	384	1	500000000000	temperate	urban, vines	unknown	2018-07-04 14:32:36.571457	2018-07-04 14:32:36.571457
56	Muunilinst	13800	28	412	1	5000000000	temperate	plains, forests, hills, mountains	25	2018-07-04 14:32:36.578137	2018-07-04 14:32:36.578137
57	Shili	unknown	unknown	unknown	1	unknown	temperate	cities, savannahs, seas, plains	unknown	2018-07-04 14:32:36.584245	2018-07-04 14:32:36.584245
58	Kalee	13850	23	378	1	4000000000	arid, temperate, tropical	rainforests, cliffs, canyons, seas	unknown	2018-07-04 14:32:36.590114	2018-07-04 14:32:36.590114
59	Umbara	unknown	unknown	unknown	unknown	unknown	unknown	unknown	unknown	2018-07-04 14:32:36.595961	2018-07-04 14:32:36.595961
60	Tatooine	10465	23	304	1 standard	200000	arid	desert	1	2018-07-04 14:32:36.601708	2018-07-04 14:32:36.601708
61	Jakku	unknown	unknown	unknown	unknown	unknown	unknown	deserts	unknown	2018-07-04 14:32:37.156704	2018-07-04 14:32:37.156704
\.


--
-- Name: planets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abdennour
--

SELECT pg_catalog.setval('public.planets_id_seq', 61, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.schema_migrations (version) FROM stdin;
20180630085005
20180630084213
20180630084158
20180630084218
20180630084209
20180630084203
20180630084227
\.


--
-- Data for Name: species; Type: TABLE DATA; Schema: public; Owner: abdennour
--

COPY public.species (id, name, classification, designation, average_height, average_lifespan, eye_colors, hair_colors, skin_colors, language, planet_id, created_at, updated_at) FROM stdin;
1	Hutt	gastropod	sentient	300	1000	yellow, red	n/a	\N	Huttese	23	2018-07-04 14:32:39.055375	2018-07-04 14:32:39.055375
2	Yoda's species	mammal	sentient	66	900	brown, green, yellow	brown, white	\N	Galactic basic	27	2018-07-04 14:32:39.591419	2018-07-04 14:32:39.591419
3	Trandoshan	reptile	sentient	200	unknown	yellow, orange	none	\N	Dosh	28	2018-07-04 14:32:40.232529	2018-07-04 14:32:40.232529
4	Mon Calamari	amphibian	sentient	160	unknown	yellow	none	\N	Mon Calamarian	30	2018-07-04 14:32:40.841721	2018-07-04 14:32:40.841721
5	Ewok	mammal	sentient	100	unknown	orange, brown	white, brown, black	\N	Ewokese	6	2018-07-04 14:32:41.460492	2018-07-04 14:32:41.460492
6	Sullustan	mammal	sentient	180	unknown	black	none	\N	Sullutese	32	2018-07-04 14:32:42.092933	2018-07-04 14:32:42.092933
7	Neimodian	unknown	sentient	180	unknown	red, pink	none	\N	Neimoidia	17	2018-07-04 14:32:42.690145	2018-07-04 14:32:42.690145
8	Gungan	amphibian	sentient	190	unknown	orange	none	\N	Gungan basic	7	2018-07-04 14:32:43.3053	2018-07-04 14:32:43.3053
9	Toydarian	mammal	sentient	120	91	yellow	none	\N	Toydarian	33	2018-07-04 14:32:43.914082	2018-07-04 14:32:43.914082
10	Dug	mammal	sentient	100	unknown	yellow, blue	none	\N	Dugese	34	2018-07-04 14:32:44.531479	2018-07-04 14:32:44.531479
11	Twi'lek	mammals	sentient	200	unknown	blue, brown, orange, pink	none	\N	Twi'leki	36	2018-07-04 14:32:45.761096	2018-07-04 14:32:45.761096
12	Aleena	reptile	sentient	80	79	unknown	none	\N	Aleena	37	2018-07-04 14:32:46.379419	2018-07-04 14:32:46.379419
13	Vulptereen	unknown	sentient	100	unknown	yellow	none	\N	vulpterish	38	2018-07-04 14:32:46.993612	2018-07-04 14:32:46.993612
14	Xexto	unknown	sentient	125	unknown	black	none	\N	Xextese	39	2018-07-04 14:32:47.600194	2018-07-04 14:32:47.600194
15	Toong	unknown	sentient	200	unknown	orange	none	\N	Tundan	40	2018-07-04 14:32:48.219024	2018-07-04 14:32:48.219024
16	Cerean	mammal	sentient	200	unknown	hazel	red, blond, black, white	\N	Cerean	42	2018-07-04 14:32:48.833001	2018-07-04 14:32:48.833001
17	Nautolan	amphibian	sentient	180	70	black	none	\N	Nautila	43	2018-07-04 14:32:49.447534	2018-07-04 14:32:49.447534
18	Zabrak	mammal	sentient	180	unknown	brown, orange	black	\N	Zabraki	44	2018-07-04 14:32:50.266962	2018-07-04 14:32:50.266962
19	Tholothian	mammal	sentient	unknown	unknown	blue, indigo	unknown	\N	unknown	45	2018-07-04 14:32:50.876686	2018-07-04 14:32:50.876686
20	Iktotchi	unknown	sentient	180	unknown	orange	none	\N	Iktotchese	46	2018-07-04 14:32:51.596187	2018-07-04 14:32:51.596187
21	Quermian	mammal	sentient	240	86	yellow	none	\N	Quermian	47	2018-07-04 14:32:52.744718	2018-07-04 14:32:52.744718
22	Kel Dor	unknown	sentient	180	70	black, silver	none	\N	Kel Dor	48	2018-07-04 14:32:53.440095	2018-07-04 14:32:53.440095
23	Chagrian	amphibian	sentient	190	unknown	blue	none	\N	Chagria	49	2018-07-04 14:32:54.054582	2018-07-04 14:32:54.054582
24	Geonosian	insectoid	sentient	178	unknown	green, hazel	none	\N	Geonosian	10	2018-07-04 14:32:54.67008	2018-07-04 14:32:54.67008
25	Mirialan	mammal	sentient	180	unknown	blue, green, red, yellow, brown, orange	black, brown	\N	Mirialan	50	2018-07-04 14:32:55.289726	2018-07-04 14:32:55.289726
26	Clawdite	reptilian	sentient	180	70	yellow	none	\N	Clawdite	53	2018-07-04 14:32:55.898254	2018-07-04 14:32:55.898254
27	Besalisk	amphibian	sentient	178	75	yellow	none	\N	besalisk	54	2018-07-04 14:32:56.51114	2018-07-04 14:32:56.51114
28	Kaminoan	amphibian	sentient	220	80	black	none	\N	Kaminoan	9	2018-07-04 14:32:57.126251	2018-07-04 14:32:57.126251
29	Skakoan	mammal	sentient	unknown	unknown	unknown	none	\N	Skakoan	55	2018-07-04 14:32:57.746748	2018-07-04 14:32:57.746748
30	Muun	mammal	sentient	190	100	black	none	\N	Muun	56	2018-07-04 14:32:59.583558	2018-07-04 14:32:59.583558
31	Togruta	mammal	sentient	180	94	red, orange, yellow, green, blue, black	none	\N	Togruti	57	2018-07-04 14:33:01.120272	2018-07-04 14:33:01.120272
32	Kaleesh	reptile	sentient	170	80	yellow	none	\N	Kaleesh	58	2018-07-04 14:33:01.735443	2018-07-04 14:33:01.735443
33	Pau'an	mammal	sentient	190	700	black	none	\N	Utapese	11	2018-07-04 14:33:01.973707	2018-07-04 14:33:01.973707
34	Wookiee	mammal	sentient	210	400	blue, green, yellow, brown, golden, red	black, brown	\N	Shyriiwook	13	2018-07-04 14:33:02.656032	2018-07-04 14:33:02.656032
36	Human	mammal	sentient	180	120	brown, blue, green, hazel, grey, amber	blonde, brown, black, red	\N	Galactic Basic	8	2018-07-04 14:33:03.271149	2018-07-04 14:33:03.271149
37	Rodian	sentient	reptilian	170	unknown	black	n/a	\N	Galactic Basic	22	2018-07-04 14:33:03.885032	2018-07-04 14:33:03.885032
35	Droid	artificial	sentient	n/a	indefinite	n/a	n/a	\N	n/a	27	2018-07-04 14:33:02.664632	2018-07-04 14:33:02.664632
\.


--
-- Name: species_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abdennour
--

SELECT pg_catalog.setval('public.species_id_seq', 37, true);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: characters characters_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.characters
    ADD CONSTRAINT characters_pkey PRIMARY KEY (id);


--
-- Name: films films_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.films
    ADD CONSTRAINT films_pkey PRIMARY KEY (id);


--
-- Name: planets planets_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.planets
    ADD CONSTRAINT planets_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: species species_pkey; Type: CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.species
    ADD CONSTRAINT species_pkey PRIMARY KEY (id);


--
-- Name: index_characters_films_on_character_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_characters_films_on_character_id ON public.characters_films USING btree (character_id);


--
-- Name: index_characters_films_on_film_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_characters_films_on_film_id ON public.characters_films USING btree (film_id);


--
-- Name: index_characters_on_planet_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_characters_on_planet_id ON public.characters USING btree (planet_id);


--
-- Name: index_characters_species_on_character_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_characters_species_on_character_id ON public.characters_species USING btree (character_id);


--
-- Name: index_characters_species_on_specie_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_characters_species_on_specie_id ON public.characters_species USING btree (specie_id);


--
-- Name: index_films_planets_on_film_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_films_planets_on_film_id ON public.films_planets USING btree (film_id);


--
-- Name: index_films_planets_on_planet_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_films_planets_on_planet_id ON public.films_planets USING btree (planet_id);


--
-- Name: index_species_on_planet_id; Type: INDEX; Schema: public; Owner: abdennour
--

CREATE INDEX index_species_on_planet_id ON public.species USING btree (planet_id);


--
-- Name: characters fk_rails_20b9aeba16; Type: FK CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.characters
    ADD CONSTRAINT fk_rails_20b9aeba16 FOREIGN KEY (planet_id) REFERENCES public.planets(id);


--
-- Name: species fk_rails_2a7543db0a; Type: FK CONSTRAINT; Schema: public; Owner: abdennour
--

ALTER TABLE ONLY public.species
    ADD CONSTRAINT fk_rails_2a7543db0a FOREIGN KEY (planet_id) REFERENCES public.planets(id);


--
-- PostgreSQL database dump complete
--


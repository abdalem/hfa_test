def init_planets(url)
  data = JSON.parse(RestClient.get(url, headers = {accept: :json})).deep_symbolize_keys
  acceptable_params = [:name, :rotation_period, :orbital_period, :diameter, :climate, :gravity, :terrain, :surface_water, :population]
  data[:results].each do |planet|
    params = {}
    acceptable_params.each do |param|
      params[param] = planet[param]
    end
    current = Planet.new(params)
    current.save(validate: false)
  end
  
  init_planets(data[:next]) if data[:next]
end

def init_species(url)
  data = JSON.parse(RestClient.get(url, headers = {accept: :json})).deep_symbolize_keys
  acceptable_params = [:name, :classification, :designation, :average_height, :hair_colors, :average_lifespan, :language, :eye_colors]
  data[:results].each do |specie|
    params = {}
    acceptable_params.each do |param|
      params[param] = specie[param]
    end
    
    if specie[:homeworld]
      planet_name = JSON.parse(RestClient.get(specie[:homeworld], headers = {accept: :json})).deep_symbolize_keys[:name]
      params[:planet] = Planet.find_by(name: planet_name)
    end

    current = Specie.new(params)
    current.save(validate: false)
  end
  
  init_species(data[:next]) if data[:next]
end

def init_characters(url)
  data = JSON.parse(RestClient.get(url, headers = {accept: :json})).deep_symbolize_keys
  acceptable_params = [:name, :height, :mass, :hair_color, :skin_color, :eye_color, :birth_year, :gender]
  data[:results].each do |character|
    params = {}
    acceptable_params.each do |param|
      params[param] = character[param]
    end

    if character[:homeworld]
      planet_name = JSON.parse(RestClient.get(character[:homeworld], headers = {accept: :json})).deep_symbolize_keys[:name]
      params[:planet] = Planet.find_by(name: planet_name)
    end

    if character[:homeworld]
      planet_name = JSON.parse(RestClient.get(character[:homeworld], headers = {accept: :json})).deep_symbolize_keys[:name]
      params[:planet] = Planet.find_by(name: planet_name)
    end

    params[:species] = character[:species].collect do |specie| 
      specie_name = JSON.parse(RestClient.get(specie, headers = {accept: :json})).deep_symbolize_keys[:name]
      Specie.find_by(name: specie_name)
    end

    current = Character.new(params)
    current.save(validate: false)
  end
  
  init_characters(data[:next]) if data[:next]
end

def init_films(url)
  data = JSON.parse(RestClient.get(url, headers = {accept: :json})).deep_symbolize_keys
  acceptable_params = [:title, :episode_id, :opening_crawl, :release_date, :director, :producer]
  data[:results].each do |film|
    params = {}
    acceptable_params.each do |param|
      params[param] = film[param]
    end

    params[:planets] = film[:planets].collect do |specie| 
      planet_name = JSON.parse(RestClient.get(specie, headers = {accept: :json})).deep_symbolize_keys[:name]
      Planet.find_by(name: planet_name)
    end

    params[:characters] = film[:characters].collect do |specie| 
      character_name = JSON.parse(RestClient.get(specie, headers = {accept: :json})).deep_symbolize_keys[:name]
      Character.find_by(name: character_name)
    end

    current = Film.new(params)
    current.save(validate: false)
  end
end

p "initialize planets"
init_planets("https://swapi.co/api/planets")
p "initialize species"
init_species("https://swapi.co/api/species")
p "initialize character"
init_characters("https://swapi.co/api/people")
p "initialize films"
init_films("https://swapi.co/api/films")
Specie.where(planet: nil).update_all(planet_id: Planet.find_by(name: "unknown").id)
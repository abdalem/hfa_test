class CreatePlanetsFilmsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :planets, :films do |t|
      t.index :planet_id
      t.index :film_id
    end
  end
end

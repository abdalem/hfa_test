class CreateCharactersSpeciesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :characters, :species do |t|
      t.index :character_id
      t.index :specie_id
    end
  end
end

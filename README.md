# README

Test réalisé pour High Flyer Agency

Application responsive de gestion de données Star Wars.

Il faut exécuter les commandes suivantes pour initialiser le projet. Les seed mettent du temps à charger (environ 3-4 min chez moi) car le script parcours la bdd de swapi pour récupérer les données les plus à jours.

    $ bundle
    $ yarn install
    $ rake db:setup
    $ ./bin/server

Naviguez ensuite vers

    $ localhost:5000
    
    
Pour lancer les test

    $ rake db:test:purge
    $ psql hfa_test_test < db/test_seeds.sql
    $ rake spec
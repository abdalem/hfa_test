$(()=>{
  let options = {
    maxHeight: 200,
    nonSelectedText: "Aucune sélection",
    buttonWidth: '100%',
    nSelectedText: " sélections",
    allSelectedText: "Tout est sélectionné"
  }

  $('#character_films').multiselect(options)
  $('#character_species').multiselect(options)
})
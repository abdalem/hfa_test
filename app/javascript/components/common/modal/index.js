import './modal'

$(() => {
  const modal_holder_selector = '#modal-container'
  const modal_selector = '.modal'
  const modal_content_selector = '.modal-content'

  $(document).on('click', 'a[data-modal]', function() {
    const location = $(this).attr('href')
    // Load modal dialog from server
    $.get(
      location,
      data => { $(modal_holder_selector).html(data).find(modal_selector).modal() }
    )
    return false
  })

  $(document).on('ajax:success', 'form[data-modal]', (event) => {
    const [data, _status, xhr] = event.detail
    const url = xhr.getResponseHeader('Location')
    console.log(data);
    console.log(_status);
    console.log(xhr);
    if (url) {
      window.location = url
    } else {
      const form = $(data).find('form').html()
      $(modal_holder_selector).find('form').empty()
      $(modal_content_selector).find('form').html(form)
    }

    return false
  })
})
$(()=>{
  let options = {
    maxHeight: 200,
    nonSelectedText: "Aucune sélection",
    buttonWidth: '100%',
    nSelectedText: " sélections",
    allSelectedText: "Tout est sélectionné"
  }

  $('#specie_films').multiselect(options)
  $('#specie_characters').multiselect(options)
})
$(()=>{
  let options = {
    maxHeight: 200,
    nonSelectedText: "Aucune sélection",
    buttonWidth: '100%',
    nSelectedText: " sélections",
    allSelectedText: "Tout est sélectionné"
  }

  $('#planet_films').multiselect(options)
  $('#planet_species').multiselect(options)
  $('#planet_characters').multiselect(options)
})
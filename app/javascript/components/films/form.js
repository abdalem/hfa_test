$(()=>{
  let options = {
    maxHeight: 200,
    nonSelectedText: "Aucune sélection",
    buttonWidth: '100%',
    nSelectedText: " sélections",
    allSelectedText: "Tout est sélectionné"
  }

  $('#film_characters').multiselect(options)
  $('#film_planets').multiselect(options)
})
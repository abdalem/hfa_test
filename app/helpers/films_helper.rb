module FilmsHelper
  def inline_films(films)
    (films.collect {|film| film.title}).join(', ')
  end

  def multiple_films(films)
    out = Film.all.order(:episode_id).collect { |film| ["Episode #{film.episode_id} - #{film.title}", film.id, selected: (films.find {|linked| linked.title == film.title}) ? true : false] }
  end
end

module SpeciesHelper
  def inline_species(species)
    (species.collect {|specie| specie.name}).join(', ')
  end

  def multiple_species(species)
    out = Specie.all.order(:name).collect { |specie| [specie.name, specie.id, selected: (species.find {|linked| linked.name == specie.name}) ? true : false] }
  end
end

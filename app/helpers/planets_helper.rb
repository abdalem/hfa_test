module PlanetsHelper
  def planet_select(current_planet)
    planet_name = current_planet ? current_planet.name : ""
    out = Planet.all.order(:name).collect { |planet| [planet.name, planet.id, selected: planet_name == planet.name] }
  end

  def multiple_planets(planets)
    out = Planet.all.order(:name).collect { |planet| [planet.name, planet.id, selected: (planets.find {|linked| linked.name == planet.name}) ? true : false] }
  end
end

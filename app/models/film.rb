class Film < ApplicationRecord
  has_and_belongs_to_many :characters
  has_and_belongs_to_many :planets
  has_many :species, -> { distinct }, through: :characters

  validates :episode_id, numericality: true
  validates :title, :opening_crawl, :release_date, presence: true
end

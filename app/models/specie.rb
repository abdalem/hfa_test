class Specie < ApplicationRecord
  belongs_to :planet
  has_and_belongs_to_many :characters
  has_many :films, -> { distinct }, through: :characters

  validates :average_height, numericality: true
  validates :name, :planet, presence: true
end

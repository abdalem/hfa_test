class Planet < ApplicationRecord
  has_and_belongs_to_many :films
  has_many :characters
  has_many :species

  validates :name, presence: true
  validates :diameter, :rotation_period, :orbital_period, :population, :surface_water, numericality: true
  before_destroy :set_planet_to_unkown

  def set_planet_to_unkown
    unknown_planet_id = Planet.find_by(name: "unknown").id
    self.characters.update_all(planet_id: unknown_planet_id)
    self.species.update_all(planet_id: unknown_planet_id)
  end
end

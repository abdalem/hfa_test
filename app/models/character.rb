class Character < ApplicationRecord
  belongs_to :planet
  has_and_belongs_to_many :species
  has_and_belongs_to_many :films

  validates :mass, :height, numericality: true
  validates :name, :planet, presence: true
end

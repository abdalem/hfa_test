class SpeciesController < ApplicationController
  before_action :set_specie, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def index
    @species = Specie.all.order(:name)
  end

  def show
    respond_modal_with @specie
  end

  def new
    @specie = Specie.new
    respond_modal_with @specie
  end

  def edit
    respond_modal_with @specie
  end

  def create
    @specie = Specie.new(specie_params)

    respond_to do |format|
      if @specie.save
        flash[:success] = "L'espèce a bien été crée"
        format.html { redirect_to species_url }
        format.json { render :show, status: :created, location: @specie }
      else
        format.html { render :new }
        format.json { render json: @specie.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @specie.update(specie_params)
        flash[:success] = "L'espèce a bien été mis à jour"
        format.html { redirect_to species_url }
        format.json { render :show, status: :ok, location: @specie }
      else
        format.html { render :edit }
        format.json { render json: @specie.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @specie.destroy
    respond_to do |format|
        flash[:success] = "L'espèce a bien été supprimé"
        format.html { redirect_to species_url }
      format.json { head :no_content }
    end
  end

  private
    def set_specie
      @specie = Specie.find(params[:id])
    end

    def specie_params
      specie_params = params.require(:specie).permit(:name, :classification, :designation, :average_height, :average_lifespan, :language, :eye_colors, :hair_colors, :skin_colors)
      specie_params[:planet] = Planet.find(params[:specie][:planet]) if params[:specie][:planet]
      specie_params[:characters] = build_characters(params[:specie][:characters]) if params[:specie][:characters]
      specie_params
    end

    def build_characters(characters)
      characters = characters - [""]
      characters.collect {|character| Character.find(character)}
    end
end

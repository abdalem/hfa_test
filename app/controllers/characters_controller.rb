class CharactersController < ApplicationController
  before_action :set_character, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def index
    @characters = Character.all.order(:name)
  end

  def show
    respond_modal_with @character
  end

  def new
    @character = Character.new
    respond_modal_with @character
  end

  def edit
    respond_modal_with @character
  end

  def create
    @character = Character.new(character_params)

    respond_to do |format|
      if @character.save
        flash[:success] = "Le personnage a bien été crée"
        format.html { redirect_to characters_url }
        format.json { head :no_content }
      else
        format.html { render :new }
        format.json { render json: @character.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @character.update(character_params)
        flash[:success] = "Le personnage a bien été mis à jour"
        format.html { redirect_to characters_url }
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.json { render json: @character.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @character.destroy
    respond_to do |format|
        flash[:success] = "Le personnage a bien été supprimé"
        format.html { redirect_to characters_url }
      format.json { head :no_content }
    end
  end

  private
    def set_character
      @character = Character.find(params[:id])
    end

    def character_params
      character_params = params.require(:character).permit(:name, :birth_year, :eye_color, :gener, :hair_color, :height, :skin_color, :mass)
      character_params[:planet] = Planet.find(params[:character][:planet]) if params[:character][:planet]
      character_params[:films] = build_films(params[:character][:films]) if params[:character][:films]
      character_params[:species] = build_species(params[:character][:species]) if params[:character][:species]
      character_params
    end

    def build_films(films)
      films = films - [""]
      films.collect {|film| Film.find(film)}
    end

    def build_species(species)
      species = species - [""]
      species.collect {|specie| Specie.find(specie)}
    end
end

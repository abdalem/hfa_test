class PlanetsController < ApplicationController
  before_action :set_planet, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def index
    @planets = Planet.all.order(:name)
  end

  def show
  end

  def new
    @planet = Planet.new
    respond_modal_with @film
  end

  def edit
    respond_modal_with @film
  end

  def create
    @planet = Planet.new(planet_params)

    respond_to do |format|
      if @planet.save
        flash[:success] = "La planète a bien été crée"
        format.html { redirect_to @planet }
        format.json { render :show, status: :created, location: @planet }
      else
        format.html { render :new }
        format.json { render json: @planet.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @planet.update(planet_params)
        flash[:success] = "La planète a bien été mis à jour"
        format.html { redirect_to @planet }
        format.json { render :show, status: :ok, location: @planet }
      else
        format.html { render :edit }
        format.json { render json: @planet.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @planet.destroy
    respond_to do |format|
      flash[:success] = "La planète a bien été supprimé"
      format.html { redirect_to planets_url }
      format.json { head :no_content }
    end
  end

  private
    def set_planet
      @planet = Planet.find(params[:id])
    end

    def planet_params
      planet_params = params.require(:planet).permit(:name, :diameter, :rotation_period, :orbital_period, :gravity, :population, :climate, :terrain, :surface_water)
      planet_params[:characters] = build_characters(params[:planet][:characters]) if params[:planet][:characters]
      planet_params[:films] = build_films(params[:planet][:films]) if params[:planet][:films]
      planet_params[:species] = build_species(params[:planet][:species]) if params[:planet][:species]
      planet_params
    end

    def build_characters(characters)
      characters = characters - [""]
      characters.collect {|character| Character.find(character)}
    end

    def build_films(films)
      films = films - [""]
      films.collect {|film| Film.find(film)}
    end

    def build_species(species)
      species = species - [""]
      species.collect {|specie| Specie.find(specie)}
    end
end

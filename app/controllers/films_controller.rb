class FilmsController < ApplicationController
  before_action :set_film, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def index
    @films = Film.all.order(:episode_id)
  end

  def show
  end

  def new
    @film = Film.new
    respond_modal_with @film
  end

  def edit
    respond_modal_with @film
  end

  def create
    @film = Film.new(film_params)

    respond_to do |format|
      if @film.save
        flash[:success] = "Le film a bien été crée"
        format.html { redirect_to @film}
        format.json { render :show, status: :created, location: @film }
      else
        format.html { render :new }
        format.json { render json: @film.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @film.update(film_params)
        flash[:success] = "Le film a bien été mis à jour"
        format.html { redirect_to @film }
        format.json { render :show, status: :ok, location: @film }
      else
        format.html { render :edit }
        format.json { render json: @film.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @film.destroy
    respond_to do |format|
      flash[:success] = "Le film a bien été supprimé"
      format.html { redirect_to films_url }
      format.json { head :no_content }
    end
  end

  private
    def set_film
      @film = Film.find(params[:id])
    end

    def film_params
      film_params = params.require(:film).permit(:title, :episode_id, :opening_crawl, :director, :producer, :release_date)
      film_params[:characters] = build_characters(params[:film][:characters]) if params[:film][:characters]
      film_params[:planets] = build_planets(params[:film][:planets]) if params[:film][:planets]
      film_params
    end

    def build_characters(characters)
      characters = characters - [""]
      characters.collect {|character| Character.find(character)}
    end

    def build_planets(planets)
      planets = planets - [""]
      planets.collect {|planet| Planet.find(planet)}
    end
end

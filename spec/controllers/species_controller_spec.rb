require 'rails_helper'

RSpec.describe SpeciesController, type: :controller do

  let(:valid_attributes) {
    {name: "Human", average_height: 178, planet: Planet.first}
  }

  let(:invalid_attributes) {
    {average_height: "abcd"}
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      specie = Specie.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      specie = Specie.create! valid_attributes
      get :show, params: {id: specie.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      specie = Specie.create! valid_attributes
      get :edit, params: {id: specie.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Specie" do
        expect {
          post :create, params: {specie: valid_attributes}, session: valid_session
        }.to change(Specie, :count).by(1)
      end

      it "redirects to the created specie" do
        post :create, params: {specie: valid_attributes}, session: valid_session
        expect(response).to redirect_to(species_url)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {specie: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:attr) {
        {name: "Not Human"}
      }

      it "updates the requested specie" do
        specie = Specie.create! valid_attributes
        put :update, params: {id: specie.to_param, specie: attr}, session: valid_session
        specie.reload
        expect(specie.name).to eq attr[:name]
      end

      it "redirects to the specie" do
        specie = Specie.create! valid_attributes
        put :update, params: {id: specie.to_param, specie: valid_attributes}, session: valid_session
        expect(response).to redirect_to(species_url)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        specie = Specie.create! valid_attributes
        put :update, params: {id: specie.to_param, specie: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested specie" do
      specie = Specie.create! valid_attributes
      expect {
        delete :destroy, params: {id: specie.to_param}, session: valid_session
      }.to change(Specie, :count).by(-1)
    end

    it "redirects to the species list" do
      specie = Specie.create! valid_attributes
      delete :destroy, params: {id: specie.to_param}, session: valid_session
      expect(response).to redirect_to(species_url)
    end
  end

end

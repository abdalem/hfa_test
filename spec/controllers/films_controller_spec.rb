require 'rails_helper'

RSpec.describe FilmsController, type: :controller do
  let(:valid_attributes) {
    {title: "Movie", episode_id: 8, opening_crawl: "opening", release_date: "10/12/2016"}
  }

  let(:invalid_attributes) {
    {episode_id: "abcd"}
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      film = Film.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      film = Film.create! valid_attributes
      get :show, params: {id: film.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      film = Film.create! valid_attributes
      get :edit, params: {id: film.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Film" do
        expect {
          post :create, params: {film: valid_attributes}, session: valid_session
        }.to change(Film, :count).by(1)
      end

      it "redirects to the created film" do
        post :create, params: {film: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Film.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {film: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:attr) {
        {title: "Updated Film"}
      }

      it "updates the requested film" do
        film = Film.create! valid_attributes
        put :update, params: {id: film.to_param, film: attr}, session: valid_session
        film.reload
        expect(film.title).to eq attr[:title]
      end

      it "redirects to the film" do
        film = Film.create! valid_attributes
        put :update, params: {id: film.to_param, film: valid_attributes}, session: valid_session
        expect(response).to redirect_to(film)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        film = Film.create! valid_attributes
        put :update, params: {id: film.to_param, film: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested film" do
      film = Film.create! valid_attributes
      expect {
        delete :destroy, params: {id: film.to_param}, session: valid_session
      }.to change(Film, :count).by(-1)
    end

    it "redirects to the films list" do
      film = Film.create! valid_attributes
      delete :destroy, params: {id: film.to_param}, session: valid_session
      expect(response).to redirect_to(films_url)
    end
  end

end

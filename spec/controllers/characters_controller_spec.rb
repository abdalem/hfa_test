require 'rails_helper'

RSpec.describe CharactersController, type: :controller do
  let(:valid_attributes) {
    {name: "Luke", height: 178, mass: 60, planet: Planet.first}
  }

  let(:invalid_attributes) {
    {height: "abcd", mass: "abcd"}
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      character = Character.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      character = Character.create! valid_attributes
      get :show, params: {id: character.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      character = Character.create! valid_attributes
      get :edit, params: {id: character.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Character" do
        expect {
          post :create, params: {character: valid_attributes}, session: valid_session
        }.to change(Character, :count).by(1)
      end

      it "redirects to the created character" do
        post :create, params: {character: valid_attributes}, session: valid_session
        expect(response).to redirect_to(characters_url)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {character: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:attr) {
        {name: "Updated Luke"}
      }

      it "updates the requested character" do
        character = Character.create! valid_attributes
        put :update, params: {id: character.to_param, character: attr}, session: valid_session
        character.reload
        expect(character.name).to eq attr[:name]
      end

      it "redirects to the character" do
        character = Character.create! valid_attributes
        put :update, params: {id: character.to_param, character: valid_attributes}, session: valid_session
        expect(response).to redirect_to(characters_url)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        character = Character.create! valid_attributes
        put :update, params: {id: character.to_param, character: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested character" do
      character = Character.create! valid_attributes
      expect {
        delete :destroy, params: {id: character.to_param}, session: valid_session
      }.to change(Character, :count).by(-1)
    end

    it "redirects to the characters list" do
      character = Character.create! valid_attributes
      delete :destroy, params: {id: character.to_param}, session: valid_session
      expect(response).to redirect_to(characters_url)
    end
  end

end

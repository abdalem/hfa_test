require 'rails_helper'

RSpec.describe PlanetsController, type: :controller do

  let(:valid_attributes) {
    {name: "Planet", diameter: 0, rotation_period: 0, orbital_period: 0, population: 0, surface_water: 0}
  }

  let(:invalid_attributes) {
    {diameter: "abcd"}
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      planet = Planet.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      planet = Planet.create! valid_attributes
      get :show, params: {id: planet.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      planet = Planet.create! valid_attributes
      get :edit, params: {id: planet.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Planet" do
        expect {
          post :create, params: {planet: valid_attributes}, session: valid_session
        }.to change(Planet, :count).by(1)
      end

      it "redirects to the created planet" do
        post :create, params: {planet: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Planet.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {planet: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:attr) {
        {name: "Updated Planet"}
      }

      it "updates the requested planet" do
        planet = Planet.create! valid_attributes
        put :update, params: {id: planet.to_param, planet: attr}, session: valid_session
        planet.reload
        expect(planet.name).to eq attr[:name]
      end

      it "redirects to the planet" do
        planet = Planet.create! valid_attributes
        put :update, params: {id: planet.to_param, planet: valid_attributes}, session: valid_session
        expect(response).to redirect_to(planet)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        planet = Planet.create! valid_attributes
        put :update, params: {id: planet.to_param, planet: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested planet" do
      planet = Planet.create! valid_attributes
      expect {
        delete :destroy, params: {id: planet.to_param}, session: valid_session
      }.to change(Planet, :count).by(-1)
    end

    it "redirects to the planets list" do
      planet = Planet.create! valid_attributes
      delete :destroy, params: {id: planet.to_param}, session: valid_session
      expect(response).to redirect_to(planets_url)
    end
  end

end

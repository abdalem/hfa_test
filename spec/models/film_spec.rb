require 'rails_helper'

RSpec.describe Film, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:characters) }
    it { should have_and_belong_to_many(:planets) }
    it { should have_many(:species) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_numericality_of(:episode_id) }
    it { should validate_presence_of(:opening_crawl) }
    it { should validate_presence_of(:release_date) }
  end
end

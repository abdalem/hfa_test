require 'rails_helper'

RSpec.describe Character, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:species) }
    it { should have_and_belong_to_many(:films) }
    it { should belong_to(:planet) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:planet) }
    it { should validate_numericality_of(:mass) }
    it { should validate_numericality_of(:height) }
  end
end
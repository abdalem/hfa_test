require 'rails_helper'

RSpec.describe Specie, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:characters) }
    it { should belong_to(:planet) }
    it { should have_many(:films) }
  end

  describe 'validations' do
    it { should validate_presence_of(:planet) }
    it { should validate_presence_of(:name) }
    it { should validate_numericality_of(:average_height) }
  end
end
require 'rails_helper'

RSpec.describe Planet, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:films) }
    it { should have_many(:characters) }
    it { should have_many(:species) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_numericality_of(:diameter) }
    it { should validate_numericality_of(:rotation_period) }
    it { should validate_numericality_of(:orbital_period) }
    it { should validate_numericality_of(:population) }
    it { should validate_numericality_of(:surface_water) }
  end

  describe "on destroy" do
    it "set planet's characters to unknown" do
      planet_id_before_destroy = Planet.find_by(name: 'Alderaan').id
      characters_ids = Planet.find_by(name: 'Alderaan').characters.collect{|character| character.id}
      Planet.find_by(name: 'Alderaan').destroy
      Character.where(id: characters_ids).each {|character| expect(character.planet_id).not_to eq planet_id_before_destroy}
    end

    it "set planet's species to unknown" do
      planet_id_before_destroy = Planet.find_by(name: 'Coruscant').id
      species_ids = Planet.find_by(name: 'Coruscant').species.collect{|specie| specie.id}
      Planet.find_by(name: 'Coruscant').destroy
      Specie.where(id: species_ids).each {|specie| expect(specie.planet_id).not_to eq planet_id_before_destroy}
    end
  end
end
Rails.application.routes.draw do
  resources :characters
  resources :films
  resources :species
  resources :planets

  root 'films#index'
end
